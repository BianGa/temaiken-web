package temaiken.vista;

import temaiken.dao.MyDBConnection;
import temaiken.servlets.Control;

public class Main {

    public static void main(String[] args) {
        MyDBConnection conn = new MyDBConnection();
        conn.getConnection();
        
        Control c = new Control();
        c.ejecutar();
    }
}
