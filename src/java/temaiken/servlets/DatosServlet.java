/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import temaiken.modelo.enums.Roles;

/**
 *
 * @author bianca
 */
public class DatosServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(Roles.CUIDADOR.getRol().equals(Controlador.getRole())){
            RequestDispatcher vista = req.getRequestDispatcher("cuidador.jsp");
            vista.forward(req, resp);
        }
        if(Roles.GUIA.getRol().equals(Controlador.getRole())){
            RequestDispatcher vista = req.getRequestDispatcher("guia.jsp");
            vista.forward(req, resp);
        }
        
    }
    
    
    
}
