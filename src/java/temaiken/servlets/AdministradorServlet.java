package temaiken.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import temaiken.bean.ItinerarioBean;
import temaiken.dao.AdministradorDAO;
import temaiken.dao.CuidadorDAO;
import temaiken.dao.ItinerarioDAO;
import temaiken.modelo.enums.JSPs;
import temaiken.modelo.enums.Opciones;

/**
 *
 * @author bianca
 */

public class AdministradorServlet extends HttpServlet {


    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response)
            throws IOException, ServletException {

        String opcion = request.getParameter(Opciones.OPCION.getOption());
        String elimino = request.getParameter(Opciones.ELIMINO.getOption());
        
        AdministradorDAO dao = new AdministradorDAO();
        
        if(Opciones.CREA_ITINERARIO.getOption().equals(opcion)){
            
        }
        if(Opciones.ELIMINA_ITINERARIO.getOption().equals(opcion)){
            ItinerarioDAO itinDao = new ItinerarioDAO();
            List<ItinerarioBean> list = new ArrayList<>();
            
            list = itinDao.searchAll();
            
            request.setAttribute("lista", list);
            
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.ELIMINA_ITINERARIO.getJsp());
            vista.forward(request, response);
        }
        
    }
}
