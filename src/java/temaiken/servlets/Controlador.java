/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.servlets;

import temaiken.dao.Modelo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import temaiken.bean.UserBean;
import temaiken.modelo.enums.JSPs;
import temaiken.modelo.enums.Roles;

/**
 *
 * @author Assad Iara
 */

public class Controlador extends HttpServlet {

    HttpServletRequest request;
    HttpServletResponse response;
    private static UserBean user;
    

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
        user = new UserBean();
        String usuario = request.getParameter("usuario");
        String password = request.getParameter("password");
        Modelo m = new Modelo();
        m.consultarUsuario(usuario,password);
        user.setUser(m.getResultado().getUser());
        user.setRol(m.getResultado().getRol());
       
        if (usuario.isEmpty() || password.isEmpty()) { 
            request.setAttribute("mensajeError", "Debe ingresar un usuario y contraseña");
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.ERROR.getJsp());
            vista.forward(request, response);
        }else if(m.getResultado().getUser() == null){ 
            request.setAttribute("mensajeError", "El usuario y/o la contraseña no son correctas.");
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.ERROR.getJsp());
            vista.forward(request, response);
        }else{ 
        if(m.getResultado().getRol().equals(Roles.ADMIN.getRol())){
            request.setAttribute("usuarios", m.getResultado());
            request.setAttribute("usuarioElegido", Controlador.getUser());
        
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.ADMINISTRADOR.getJsp());
            vista.forward(request, response);
        }
        else if(m.getResultado().getRol().equals(Roles.CUIDADOR.getRol()) ){
            request.setAttribute("usuarios", m.getResultado());
            request.setAttribute("usuarioElegido", Controlador.getUser());
        
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.CUIDADOR.getJsp());
            vista.forward(request, response);
        }else{
            request.setAttribute("usuarioElegido", Controlador.getUser());
        
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.GUIA.getJsp());
            vista.forward(request, response);
        }}
    }

    public static String getUser(){
        return user.getUser();
    }
    
    public static String getRole(){
        return user.getRol();
    }
    
}
