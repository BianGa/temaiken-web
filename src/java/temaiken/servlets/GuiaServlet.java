 /* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates /*

 * and open the template in the editor.
 */
package temaiken.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import temaiken.bean.ItinerarioBean;
import temaiken.dao.GuiaDAO;
import temaiken.dao.ItinerarioDAO;
import temaiken.modelo.enums.JSPs;
import temaiken.modelo.enums.Opciones;

/**
 *
 * @author bianca
 */

public class GuiaServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response)
            throws IOException, ServletException {
        
        String opcion = request.getParameter("opcion");
        GuiaDAO m = new GuiaDAO();
        m.consultarDatos(Controlador.getUser());

       if(Opciones.CONSULTAR_DATOS.getOption().equals(opcion)){
           request.setAttribute("nombre", m.getResultado().getNombre());
           request.setAttribute("direccion", m.getResultado().getDireccion());
           request.setAttribute("telefono", m.getResultado().getTelefono());
           request.setAttribute("fechaIngreso", m.getResultado().getFechaIngreso());
            //busqueda en la base de los datos del usuario  
            RequestDispatcher vista = request.getRequestDispatcher("datos.jsp");
            vista.forward(request, response);
        }
       if(Opciones.CONSULTAR_ITINERARIOS.getOption().equals(opcion)){
           List<ItinerarioBean> beanList = new ArrayList<>();
            ItinerarioDAO dao = new ItinerarioDAO();
            
            //TODO terminar y probar el DAO
            beanList = dao.getItinerarioByUser(Controlador.getUser());
            request.setAttribute("user", Controlador.getUser().toUpperCase());
            
            request.setAttribute("itinerario", beanList);
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.ITINERARIOS.getJsp());
            vista.forward(request, response);
       }
       if(Opciones.SALIR.getOption().equals(opcion)){
           RequestDispatcher vista = request.getRequestDispatcher(JSPs.INDEX.getJsp());
           vista.forward(request, response);
       }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher vista = req.getRequestDispatcher(JSPs.GUIA.getJsp());
        vista.forward(req, resp);
    }
    
    
}
