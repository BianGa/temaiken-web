package temaiken.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import temaiken.dao.ItinerarioDAO;
import temaiken.modelo.enums.JSPs;
import temaiken.modelo.enums.Mensajes;
import temaiken.modelo.enums.Opciones;

/**
 *
 * @author bianca
 */
public class EliminarServlet extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String option = req.getParameter(Opciones.OPCION.getOption());
                
        if(Opciones.ELIMINA_ITINERARIO.getOption().equals(option)){
            ItinerarioDAO dao = new ItinerarioDAO();
            int result = -1;
            result = dao.deleteItinerarioByCode(option);
            if(result > 0){
                req.setAttribute(Opciones.MENSAJE.getOption(), Mensajes.ITINERARIO_ELIMINADO.getMsg());
            } else {
                req.setAttribute(Opciones.MENSAJE.getOption(), Mensajes.ERROR_ITINERARIO_ELIMINADO.getMsg());
            }
            
            RequestDispatcher vista = req.getRequestDispatcher(JSPs.MENSAJE.getJsp());
            vista.forward(req, resp);
        }
    }
    
}
