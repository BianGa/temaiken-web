/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bianca
 */
public class MyDBConnection {
    
    /**
     * CONFIGURACION DE BIAN
     */
    private static final String base = "jdbc:mysql://";
    private static final String host = "localhost:3306/";
    private static final String schema = "parque";
    private static final String usr = "root";
    private static final String pass = "root";
    private static Connection conn;
    
    public static Connection getConnection(){
        if(conn == null){
            try {
                conn = DriverManager.getConnection(base+host+schema, usr, pass);
            } catch (SQLException ex) {
                Logger.getLogger(MyDBConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return conn;
    }
    
}
