/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.dao;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import temaiken.bean.UserBean;
/**
 *
 * @author 
 */
public class GuiaDAO {

    private UserBean resultado;

    public GuiaDAO() {
        resultado = new UserBean();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GuiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void consultarDatos(String u) {
        try {
            Statement st = MyDBConnection.getConnection().createStatement();
            st.executeQuery("SELECT nombre, direccion, telefono, fechaIngreso "
                    + "FROM usuarios WHERE usuario='" + u + "';");
            ResultSet rs = st.getResultSet();
            
            while(rs.next()){
                UserBean user = new UserBean();
                user.setNombre(rs.getString(1));
                user.setDireccion(rs.getString(2));
                user.setTelefono(rs.getString(3));
                user.setFechaIngreso(rs.getString(4));
                resultado = user;
            }
            
            st.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(GuiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public UserBean getResultado() {
        return resultado;
    }

}

