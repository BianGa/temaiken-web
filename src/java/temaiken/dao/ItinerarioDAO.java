/*
 * 
 * 
 * 
 */
package temaiken.dao;

import com.mysql.cj.jdbc.result.ResultSetFactory;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import temaiken.bean.ItinerarioBean;

/**
 *
 * @author bianca
 */
public class ItinerarioDAO {
    
    ItinerarioBean itinerario;
    List<ItinerarioBean> lItinerario;
    
    /**
     * Class.forName("com.mysql.jdb.Driver") es para que reconozca el tipo de conexion jdbc
     */
    public ItinerarioDAO(){
        itinerario = new ItinerarioBean();
        lItinerario = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GuiaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<ItinerarioBean> getItinerarioByUser(String user){
        
        ItinerarioBean bean = new ItinerarioBean();

        String query = "SELECT i.codigo, i.duracion, i.longitud, i.max_visitantes, i.num_especies, i.horario "
                + "FROM itinerario i "
                + "JOIN usuario_itinerario ui on i.codigo = ui.codigo_itin "
                + "WHERE ui.usuario = '" + user + "';";
        
        try {
            Statement stmt = MyDBConnection.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                bean.setCodigo(Long.valueOf(rs.getString(1)));
                bean.setDuracion(rs.getString(2));
                bean.setLongitud(rs.getString(3));
                bean.setMaxVisitantes(Integer.valueOf(rs.getString(4)));
                bean.setNumEspecies(Integer.valueOf(rs.getString(5)));
                bean.setHorario(rs.getString(6));
                lItinerario.add(bean);
            }
        
            for(ItinerarioBean itinerario : lItinerario){
                itinerario.setZonasVisita(getZonasVisitaForItinerario(bean.getCodigo()));
            }
       
        } catch (SQLException ex) {
            Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lItinerario;
    }
    
    private List<String> getZonasVisitaForItinerario(Long codigoItin) throws SQLException{
        List<String> zonas = new ArrayList<>();
        
        String query = "SELECT z.nombre FROM zonas z "
                + "RIGHT JOIN itinerario_zona iz on z.id = iz.id_zona "
                + "RIGHT JOIN itinerario i on i.id_zonas_visita = iz.id_zonas_visita "
                + "WHERE i.codigo = " + codigoItin + ";";
        
        Statement stmt = MyDBConnection.getConnection().createStatement();
        ResultSet rs = stmt.executeQuery(query);
        
        while(rs.next()){
            zonas.add(rs.getString(1));
        }
        
        return zonas;
    }
    
    public List<ItinerarioBean> searchAll(){
        List<ItinerarioBean> lista = new ArrayList<>();
        
        String query = "SELECT codigo, duracion, longitud, max_visitantes, horario "
                + "FROM itinerario;";
        
        try {
            Statement stmt = MyDBConnection.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                itinerario = new ItinerarioBean();
                itinerario.setCodigo(Long.valueOf(rs.getString(1)));
                itinerario.setDuracion(rs.getString(2));
                itinerario.setLongitud(rs.getString(3));
                itinerario.setMaxVisitantes(Integer.valueOf(rs.getString(4)));
                itinerario.setHorario(rs.getString(5));
                lista.add(itinerario);
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public int deleteItinerarioByCode(String cod){
        String query = "DELETE FROM itinerario WHERE codigo = '" + cod + "';";
        String[] rows = {"codigo", "duracion", "longitud", "max_visitantes", "num_especies", "horario", "id_zonas_vista"};
        int result = -1;
        try {
            Statement stmt = MyDBConnection.getConnection().createStatement();
            result = stmt.executeUpdate(query, rows);
        } catch (SQLException ex) {
            Logger.getLogger(ItinerarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
}
