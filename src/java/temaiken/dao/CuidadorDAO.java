/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.dao;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import temaiken.bean.UserBean;
/**
 *
 * @author iara
 */
public class CuidadorDAO {

    private UserBean resultado;

    public CuidadorDAO() {
        resultado = new UserBean();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CuidadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void consultarDatos(String u) {
        try {
            Statement stmt = MyDBConnection.getConnection().createStatement();
            stmt.execute(
                    "SELECT nombre, direccion, telefono, fechaIngreso "
                            + "FROM usuarios "
                            + "WHERE usuario='" + u + "';");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                UserBean usuario = new UserBean();
                usuario.setNombre(rs.getString(1));
                usuario.setDireccion(rs.getString(2));
                usuario.setTelefono(rs.getString(3));
                usuario.setFechaIngreso(rs.getString(4));
                resultado = usuario;
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public UserBean getResultado() {
        return resultado;
    }

}

