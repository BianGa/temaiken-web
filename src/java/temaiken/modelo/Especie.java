/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Iara
 */
public class Especie implements Serializable{
    private String nombreEspanol;
    private String nombreCientifico;
    private String descripcion;
    private ArrayList<Habitat> habitats;

    public Especie(String nombreEspanol, String nombreCientifico, String descripcion) {
        this.nombreEspanol = nombreEspanol;
        this.nombreCientifico = nombreCientifico;
        this.descripcion = descripcion;
        this.habitats = new ArrayList<>();
    }

    /**
     * @return the nombreEspanol
     */
    public String getNombreEspanol() {
        return nombreEspanol;
    }

    /**
     * @param nombreEspanol the nombreEspanol to set
     */
    public void setNombreEspanol(String nombreEspanol) {
        this.nombreEspanol = nombreEspanol;
    }

    /**
     * @return the nombreCientifico
     */
    public String getNombreCientifico() {
        return nombreCientifico;
    }

    /**
     * @param nombreCientifico the nombreCientifico to set
     */
    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the habitats
     */
    public ArrayList<Habitat> getHabitats() {
        return habitats;
    }

    /**
     * @param habitats the habitats to set
     */
    public void setHabitats(ArrayList<Habitat> habitats) {
        this.habitats = habitats;
    }
    
}
