/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Iara
 */
public class Temaiken implements Serializable {

    private ArrayList<Especie> especies;
    private ArrayList<Usuario> usuarios;
    private ArrayList<Zona> zonas;
    
    public Temaiken(){
        this.especies = new ArrayList<>();
        this.usuarios = new ArrayList<>();
        this.zonas = new ArrayList<>();
    }
    
    public Temaiken(ArrayList<Especie> especies, ArrayList<Usuario> usuarios,
            ArrayList<Zona> zonas){
        this.especies = especies;
        this.usuarios = usuarios;
        this.zonas = zonas;
    }
    
    public Usuario buscarUsuario(String datos) {
        int i = 0;
        boolean encontrado = false;
        Usuario u = null;

        while (i < usuarios.size() && !encontrado) {
            u = usuarios.get(i); //usuarios[i]
            if (datos.equals(u.getUsuario() + ":" + u.getPassword())) {
                encontrado = true;
            } else {
                i++;
            }
        }
        
        if (encontrado) {
            return u;
        } else {
            return null;
        }
    }
    
    public Temaiken deSerializar(String a) throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(a);
        ObjectInputStream o = new ObjectInputStream(f);
        Temaiken t = (Temaiken) o.readObject();
        o.close();
        f.close();
        return t;
    }
    //serializo porque quiero guardar la informacion que genero en el programa, para que cuando cierre y abra el programa, tenga todo. 
    //serializa al objeto entero(temaiken, y todos los objetos que lo componen) en una cadena bytes
    //esto es necesario porque cuando se van creando objetos en memoria se crean todos desparramados en el heap, y con la funcion serializar lor ordeno en una cadena de bytes
    //después la cadena de bytes la puedo guardar en un archivo.
    public void serializar(String a) throws IOException {
        FileOutputStream f = new FileOutputStream(a);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
        f.close();
    }

    /**
     * @return the especies
     */
    public ArrayList<Especie> getEspecies() {
        return especies;
    }

    /**
     * @param especies the especies to set
     */
    public void setEspecies(ArrayList<Especie> especies) {
        this.especies = especies;
    }

    /**
     * @return the usuarios
     */
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * @return the zonas
     */
    public ArrayList<Zona> getZonas() {
        return zonas;
    }

    /**
     * @param zonas the zonas to set
     */
    public void setZonas(ArrayList<Zona> zonas) {
        this.zonas = zonas;
    }

    String mostrarGuias() {
        String lista = "";
        for(int i = 0; i < this.usuarios.size(); i++) {
            Usuario u = this.usuarios.get(i);
            if(u instanceof Guia){
                lista += " " + i + " - " + u.getNombre() + " " + u.getFechaIngreso() + "\n";
            }
        }
        
        return lista;
    }

    String mostrarZonas() {
        String lista = "";
        for(int i = 0; i < this.zonas.size(); i++) {
            Zona z = this.zonas.get(i);
            lista += " " + i + " - " + z.getNombre() + "\n";
        }
        
        return lista;
    }
    
    String mostrarEspecies() {
        String lista = "";
        for(int i = 0; i < this.especies.size(); i++) {
            Especie e = this.especies.get(i);
            lista += " " + i + " - " + e.getNombreCientifico()+ "\n";
        }
        
        return lista;
    }

    String mostrarCuidadores() {
        String lista = "";
        for (int i = 0; i < usuarios.size(); i++) {
            Usuario u = usuarios.get(i);
            if(u instanceof Cuidador){
                lista += " " + i + " - " + u.getNombre() + " " + u.getFechaIngreso() + "\n";
            }
            
        }
        
        return lista;
    }

           
}
