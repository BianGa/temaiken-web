package temaiken.modelo.enums;

/**
 *
 * @author bianca
 */
public enum Mensajes {
    
    ITINERARIO_ELIMINADO ("El itinerario seleccionado ha sido eliminado."),
    ERROR_ITINERARIO_ELIMINADO
            ("Ocurrió algún problema y el itinerario no pudo ser eliminado.\nPor favor contacte a su provedor.");
    
    private String msg;
    
    Mensajes (String msg){
        this.msg = msg;
    }
    
    public String getMsg(){
        return this.msg;
    }
}
