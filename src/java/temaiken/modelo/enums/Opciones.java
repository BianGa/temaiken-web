/*
 * 
 * 
 * 
 */
package temaiken.modelo.enums;

/**
 *
 * @author bianca
 */
public enum Opciones {
    
    OPCION ("option"),
    ELIMINO ("elimino"),
    MENSAJE ("mensaje"),
    CONSULTAR_DATOS ("datos"),
    CONSULTAR_ITINERARIOS ("consultarItinerarios"),
    CONSULTAR_ESPECIES ("consultarEspecie"),
    CREA_ITINERARIO ("creaItinerario"),
    ELIMINA_ITINERARIO ("elminaItinerario"),
    CREA_ESPECIE ("asignaEspecie"),
    ELIMINA_ESPECIE ("eliminaEspecie"),
    CREA_ZONA ("nuevaZona"),
    ELIMINA_ZONA ("eliminaZona"),
    ALTA_USUARO ("altaUsuario"),
    BAJA_USUARIO ("bajaUsuario"),
    SALIR ("salir");
    
    private String option;
    
    Opciones (String op){
        this.option = op;
    }

    public String getOption(){
        return this.option;
    }
    
}
