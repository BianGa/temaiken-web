package temaiken.modelo.enums;

/**
 * Lista de archivos jsp
 * @author bianca
 */
public enum JSPs {
    
    INDEX ("index.jsp"),
    GUIA ("guia.jsp"),
    DATOS ("datos.jsp"),
    ITINERARIOS ("itinerarios.jsp"),
    CUIDADOR ("cuidador.jsp"),
    CUIDADOR_ESPECIES ("cuidadorEspecies.jsp"),
    ADMINISTRADOR ("administrador.jsp"),
    ELIMINA_ITINERARIO ("eliminarItinerario.jsp"),
    MENSAJE ("mensaje.jsp"),
    ERROR ("error.jsp");
    
    private String jsp;

    private JSPs(String jsp) {
        this.jsp = jsp;
    }
    
    public String getJsp(){
        return this.jsp;
    }
    
}
