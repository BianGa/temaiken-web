/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.time.LocalDate;
import java.time.LocalTime;
import temaiken.vista.EntradaSalida;


/**
 *
 * @author Iara
 */
public class Administrador extends Usuario{
    
    public Administrador(String nombre, String direccion, String telefono,
            LocalDate fechaIngreso, String usuario, String password) {
        super(nombre, direccion, telefono, fechaIngreso, usuario, password);
    }

    @Override
    public void proceder(Temaiken temaiken) {
        char opcion;
        
        EntradaSalida.mostrarString("Bienvenido administrador " + nombre);
        
        do{
            do {
                opcion = EntradaSalida.leerChar(
                        "1 - Crear itinerario\n" +
                        "2 - Eliminar itinerario\n" +
                        "3 - Asignar especie\n" +
                        "4 - Desasignar especie\n" +
                        "5 - Salir del programa\n"
                );
            }
            while(opcion > '5' || opcion < '1');

            switch(opcion) {
                case '1':
                    crearItinerario(temaiken);
                    break;
                case '2':
                    eliminarItinerario(temaiken);
                    break;
                case '3':
                    asignarEspecie(temaiken);
                    break;
                case '4':
                    desasignarEspecie(temaiken);
                    break;
            }
        }
        while(opcion != '5');
    }

    private void crearItinerario(Temaiken temaiken) {
        int codigo = EntradaSalida.leerInt("Ingrese el código");
        int duracion = EntradaSalida.leerInt("Ingrese la duración");
        int longitud = EntradaSalida.leerInt("Ingrese la longitud");
        int maxVisitantes = EntradaSalida.leerInt("Ingrese la cantidad máxima de visitantes");
        
        String listaGuias = temaiken.mostrarGuias();
        listaGuias += "\nIngrese el numero de guía";
        
        Guia guia = null;
        Usuario usuario = null;
        
        while(guia == null){
            int numeroGuia = EntradaSalida.leerInt(listaGuias);
            
            if(numeroGuia >= 0 && numeroGuia < temaiken.getUsuarios().size()){
                usuario = temaiken.getUsuarios().get(numeroGuia);
                if(usuario instanceof Guia) {
                    guia = (Guia) usuario;
                } 
            }
        }
        
        Itinerario itinerario = new Itinerario(codigo, duracion, longitud, maxVisitantes);
        
        String listaZonas = temaiken.mostrarZonas();
        listaZonas += "\nIngrese el numero de zona";
        boolean otraZona = true;
        
        while(otraZona){
            Zona zona = null;
        
            while(zona == null){
                int numeroZona = EntradaSalida.leerInt(listaZonas);

                if(numeroZona >= 0 && numeroZona < temaiken.getZonas().size()){
                    zona = temaiken.getZonas().get(numeroZona); 
                }
            }
            
            // Falta validar que no se ingresen zonas repetidas
            itinerario.getZonas().add(zona);
            //zona = null;
            
            otraZona = EntradaSalida.leerBoolean("Quiere ingresar otra zona?");
        }
        
        LocalTime horario = EntradaSalida.leerHora("Ingrese la hora en el siguiente formato hh:mm :");
               
        GuiaItinerario guiaItinerario =  new GuiaItinerario(horario, itinerario);
        guia.getGuiaItinerarios().add(guiaItinerario);
    }    

    private void asignarEspecie(Temaiken temaiken) {
        
        String listaCuidadores = temaiken.mostrarCuidadores();
        listaCuidadores += "\nIngrese el numero de cuidador";
        
        Cuidador cuidador = null;
        Usuario usuario = null;
        
        while(cuidador == null){
            int numeroCuidador = EntradaSalida.leerInt(listaCuidadores);
            
            if(numeroCuidador >= 0 && numeroCuidador < temaiken.getUsuarios().size()){
                usuario = temaiken.getUsuarios().get(numeroCuidador);
                
                //me aseguro que el usuario sea un cuidador
                if(usuario instanceof Cuidador) {
                    //no es licito asignar a un objeto de tipo cuidador, 
                    //un usuario sin castearlo a cuidador
                    cuidador = (Cuidador) usuario;//si el usuario no es un cuidador el casteo explota
                } 
            }
        }
        
        String listaEspecies = temaiken.mostrarEspecies();
        listaEspecies += "\nIngrese el numero de especie";
        
        Especie especie = null;
        
        while(especie == null){
            int numeroEspecie = EntradaSalida.leerInt(listaEspecies);
            
            if(numeroEspecie >= 0 && numeroEspecie < temaiken.getEspecies().size()){
                especie = temaiken.getEspecies().get(numeroEspecie);
            }
        }
       
        LocalDate fecha = EntradaSalida.leerFecha("Ingrese fecha en el siguiente formato aaaa-mm-dd");
               
        CuidadorEspecie cuidadorEspecie =  new CuidadorEspecie(fecha, especie);
        cuidador.getCuidadorEspecies().add(cuidadorEspecie);
    }

    private void eliminarItinerario(Temaiken temaiken) {
        String listaGuias = temaiken.mostrarGuias();
        listaGuias += "\nIngrese el número de guía para ver sus itinerarios:";
        
        Guia guia = null;
        Usuario usuario = null;
        
        while(guia == null){
            int numeroGuia = EntradaSalida.leerInt(listaGuias);
            
            if(numeroGuia >= 0 && numeroGuia < temaiken.getUsuarios().size()){
                usuario = temaiken.getUsuarios().get(numeroGuia);
                if(usuario instanceof Guia) {
                    guia = (Guia) usuario;
                } 
            }
        }
        
        String listaItinerarios = guia.listarItinerarios();
        listaItinerarios += "\nSeleccione el itinerario que desea borrar";
        
        int numeroItinerario = -1;
        
        while(!(numeroItinerario >= 0 && numeroItinerario < guia.getGuiaItinerarios().size())){
            numeroItinerario = EntradaSalida.leerInt(listaItinerarios); 
        }
        guia.getGuiaItinerarios().remove(numeroItinerario);
    }

    private void desasignarEspecie(Temaiken temaiken) {
        
        String listaCuidadores = temaiken.mostrarCuidadores();
        listaCuidadores += "\nIngrese el número de cuidador para ver sus especies asignadas:";
        
        Cuidador cuidador = null;
        Usuario usuario = null;
        
        while(cuidador == null){
            int numeroCuidador = EntradaSalida.leerInt(listaCuidadores);
            
            if(numeroCuidador >= 0 && numeroCuidador < temaiken.getUsuarios().size()){
                usuario = temaiken.getUsuarios().get(numeroCuidador);
                if(usuario instanceof Cuidador){
                    cuidador = (Cuidador) usuario;
                }
            }
        }
       
        String listaEspecies = cuidador.listarEspecies();
        listaEspecies += "\nSeleccione la especie que desea desasignar";
        
        int numeroEspecie = -1;
        
        while(!(numeroEspecie >= 0 && numeroEspecie < cuidador.getCuidadorEspecies().size())){
            numeroEspecie = EntradaSalida.leerInt(listaEspecies);
        }
        cuidador.getCuidadorEspecies().remove(numeroEspecie);
    }

    private void salirPrograma(Temaiken temaiken) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
