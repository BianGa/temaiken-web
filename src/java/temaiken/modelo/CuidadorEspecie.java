/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author Iara
 */
class CuidadorEspecie implements Serializable{
    private LocalDate fecha;
    private Especie especie;
    
    public CuidadorEspecie(LocalDate fecha, Especie especie){
        this.fecha = fecha;
        this.especie = especie;
    }

    /**
     * @return the fecha
     */
    public LocalDate getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the especie
     */
    public Especie getEspecie() {
        return especie;
    }

    /**
     * @param especie the especie to set
     */
    public void setEspecie(Especie especie) {
        this.especie = especie;
    }

    String detallar() {
        return "Nombre español: " + especie.getNombreEspanol() + "\n" +
               "Nombre cientifico: " + especie.getNombreCientifico() + "\n" +
               "Descripción: " + especie.getDescripcion() + "\n";
    }
}
