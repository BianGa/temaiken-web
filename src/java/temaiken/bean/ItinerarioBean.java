/*
 * 
 * 
 * 
 */
package temaiken.bean;

import java.util.List;

/**
 * @param duracion -> tiempo que dura el itinerario en horas
 * @param longitud -> largo del itinerario en km
 * @author bianca
 */
public class ItinerarioBean {
    
    private Long codigo;
    private String duracion;
    private String longitud;
    private Integer maxVisitantes;
    private Integer numEspecies;
    private List<String> zonasVisita;
    private String horario;

    public List<String> getZonasVisita() {
        return zonasVisita;
    }

    public void setZonasVisita(List<String> zonasVisita) {
        this.zonasVisita = zonasVisita;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public Integer getMaxVisitantes() {
        return maxVisitantes;
    }

    public void setMaxVisitantes(Integer maxVisitantes) {
        this.maxVisitantes = maxVisitantes;
    }

    public Integer getNumEspecies() {
        return numEspecies;
    }

    public void setNumEspecies(Integer numEspecies) {
        this.numEspecies = numEspecies;
    }
    
    
}
