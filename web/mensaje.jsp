<%-- 
    Document   : error
    Created on : 14-sep-2018, 20:50:07
    Author     : andra
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Bioparque Temaiken</title>
    </head>
    <body>
        <div class="container" align="center">
                <h1>${mensaje}</h1>
                <div>${mensajeError}</div>
                <a href="./" class="btn btn-secondary">Volver</a>
        </div>
    </body>
</html>
