<%-- 
    Document   : datos
    Created on : 29-sep-2018, 18:10:05
    Author     : coppel1
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Bioparque Temaiken</title>
    </head>
    <body>
        <header><h1>Datos del usuario</h1></header>
        <div class="container" align="center">
            <form method="get" action="datos">
                <section class="about">
                    <p class="centrar">
                        <li>Nombre: ${nombre}</li>
                        <li>Dirección: ${direccion}</li>
                        <li>Teléfono: ${telefono}</li>
                        <li>Fecha de Ingreso: ${fechaIngreso}</li>
                    </p>
                    <input type="submit" value="volver" name="back" />
            </form>
        </div>
    </body>
</html>
