<%-- 
    Document   : itinerarios
    Created on : 22/10/2018, 20:38:03
    Author     : bianca
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Bioparque Temaikèn</title>
    </head>
    <body>
        <header><h1>Itinerarios</h1></header>
        <div class="container" align="left">  
            <form method="get" action="guia">
                <h1 align="center">${user}, estos son tus itinerarios:</h1>
                <c:forEach items="${itinerario}" var="item">
                    <section class="about">
                        <h2>Itinerario Codigo : ${item.codigo}</h2>
                        <p>
                        <li>Duración : ${item.duracion}hs</li>
                        <li>Longitud : ${item.longitud}km</li>
                        <li>Máx. Vis. Autorizados : ${item.maxVisitantes}</li>
                        <li>Núm. Especies : ${item.numEspecies}</li>
                        <li>Horario : ${item.horario}hs</li>
                        <li>Zonas : 
                            <c:forEach items="${item.zonasVisita}" var="zonas">
                                ${zonas} -  
                            </c:forEach>
                        </li>
                        </p>
                    </section>
                </c:forEach>
            </form>
            <form method="get" action="datos">
                <div align="center">
                    <input type="submit" value="Volver" name="back" />
                </div>
            </form>
        </div>
    </body>
</html>
