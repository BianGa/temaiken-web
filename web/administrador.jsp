<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Bioparque Temaiken</title>
    </head>
    <body>
        <header><h1>Menú de Administrador</h1></header>
        <form method="post" action="administrador">
            <h2 align="center">Bienvenido administrador ${usuarioElegido}!</h2>
            <div class="container" align="center">
                <p>
                    Seleccione la opción deseada:
                    <select name="option" size="1">
                        <option value="creaItinerario"> Crear itinerario </option>
                        <option value="elminaItinerario"> Eliminar itinerario </option>
                        <option value="asignaEspecie"> Asignar especie </option>
                        <option value="eliminaEspecie"> Eliminar especie </option>
                        <option value="nuevaZona"> Crear nueva zona </option>
                        <option value="eliminaZona"> Eliminar zona </option>
                        <option value="altaUsuario"> Dar de alta Cuidador/Guía </option>
                        <option value="bajaUsuario"> Dar de baja Cuidador/Guía </option>
                        <option value="salir"> Salir </option>
                    </select>
                    <input type="submit" value ="Enviar" />
                </p>
            </div>
        </form>
    </body>
</html>