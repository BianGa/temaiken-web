<%-- 
    Document   : eliminarItinerario
    Created on : 26/10/2018, 01:48:55
    Author     : bianca
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Bioparque Temaiken</title>
    </head>
    <body>
        <header><h1>Menú de Administrador</h1></header>
        <form method="post" action="eliminar">
            <div class="container" align="center">
                <p>
                    Seleccione el itinerario que desea eliminar:
                    <select class="interests" name="option" size="1">
                        <c:forEach items="${lista}" var="itinerario">
                            <option value="${itinerario.codigo}">
                                Itinerario: ${itinerario.codigo} - Duración: ${itinerario.duracion}hs -
                                Horario: ${itinerario.horario} - Máx. Visitantes: ${itinerario.maxVisitantes}
                            </option>
                        </c:forEach> 
                    </select>
                    <input type="hidden" name="elimino" value="itinerario" >
                    <input type="submit" value ="Enviar" >
                </p>
            </div>
        </form>
        //TODO llamados al dao para eliminar por native query
    </body>
</html>
