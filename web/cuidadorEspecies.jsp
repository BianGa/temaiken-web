<%-- 
    Document   : cuidadorEspecies
    Created on : 25/10/2018, 23:59:49
    Author     : bianca
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Bioparque Temaikèn</title>
    </head>
    <body>
        <header><h1>Especies</h1></header>
        <div class="container" align="left">
            <form method="get" action="cuidador">
                <h1 align="center">${user}, estas son</h1>
                <h1 align="center">las especies a tu cuidado:</h1>
                <c:forEach items="${listaEspecies}" var="especie">
                    <section class="about">
                        <h2 align="center">${especie.especie}</h2>
                        <p>
                        <li>Nombre científico: ${especie.nombreCientifico}</li>
                        <li>Descripción: ${especie.descripcion}</li>
                        <li>Es cuidado por tí desde: ${especie.esCuidadoDesde}</li>
                        </p>
                    </section>
                </c:forEach>
            </form>
            <form method="get" action="datos">
                <div align="center">
                    <input type="submit" value="Volver" name="back" />
                </div>
            </form>
        </div>
    </body>
</html>
